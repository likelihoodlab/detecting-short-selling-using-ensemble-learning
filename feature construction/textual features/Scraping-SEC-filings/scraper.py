import requests, os
from bs4 import BeautifulSoup as BeautifulSoup
from selenium import webdriver
import pandas as pd
import csv
import time

def get_list(ticker, start_year, cik = None,):
    #获取年报存放的地址
    base_url_part1 = "http://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK="
    base_url_part2 = "&type=20-F&dateb=&owner=&count=100&output=xml"
    href = []

    try:
        if not cik:
            base_url = base_url_part1 + ticker + base_url_part2
        else:
            base_url = base_url_part1 + cik + base_url_part2 + base_url_part3
        print("The base url of", ticker, "is" ,base_url)
        requests.adapters.DEFAULT_RETRIES = 5  # 增加重连次数
        s = requests.session()
        s.keep_alive = False  # 关闭多余连接
        sec_page = s.get(base_url, timeout = 5)
        time.sleep(1)
        sec_soup = BeautifulSoup(sec_page.content, 'lxml')

        filings = sec_soup.findAll('filing')

        for filing in filings:
            report_year = int(filing.datefiled.get_text()[0:4])
            if (filing.type.get_text() == "20-F") & (report_year > start_year):
                print(filing.filinghref.get_text())
                href.append((filing.filinghref.get_text(), '20-F', report_year,ticker))
    except:
        pass
    if not len(href):
        base_url_part2 = "&type=10-K&dateb=&owner="
        if not cik:
            base_url = base_url_part1 + ticker + base_url_part2 + base_url_part3
        else:
            base_url = base_url_part1 + cik + base_url_part2 + base_url_part3
        print("The base url of", ticker, "is", base_url)
        requests.adapters.DEFAULT_RETRIES = 5  # 增加重连次数
        s = requests.session()
        s.keep_alive = False  # 关闭多余连接
        sec_page = s.get(base_url, timeout = 5)
        time.sleep(1)
        sec_soup = BeautifulSoup(sec_page.content, 'lxml')
        filings = sec_soup.findAll('filing')

        for filing in filings:
            report_year = int(filing.datefiled.get_text()[0:4])
            if (filing.type.get_text() == "10-K") & (report_year > start_year):
                print(filing.filinghref.get_text())
                href.append((filing.filinghref.get_text(), '10-K', report_year, ticker))
    return href

def download_report(url_list, dir_path, getCsv = False):
    """
    :param url_list: 年报存放的地址
    :param dir_path: 下载文件的存放地址，如果是只需要下载表格那就是表格的地址
    :param getCsv: True：返回一个记录了html网址的表格，False：下载html文件
    """
    target_base_url = 'http://www.sec.gov'
    
    for report_url, report_type, report_year, ticker in url_list:
        requests.adapters.DEFAULT_RETRIES = 5  # 增加重连次数
        s = requests.session()
        s.keep_alive = False  # 关闭多余连接
        report_page = s.get(report_url, timeout = 5)
        time.sleep(1)
        report_soup = BeautifulSoup(report_page.content, 'lxml')
        xbrl_file = report_soup.findAll('tr')
        for item in xbrl_file:
            try:
                if item.findAll('td')[3].get_text() == report_type:
                    if not os.path.exists(dir_path):
                        os.makedirs(dir_path)

                    target_url = target_base_url + item.findAll('td')[2].find('a')['href']
                    target_url = "/".join(report_url.split('/')[0:-1]) + '/'+ target_url.split('/')[-1]
                    print("Target URL found!")
                    print("Target URL is:", target_url)
                    if not getCsv:
                        file_name = ticker + '-' + str(report_year - 1) + '.htm'
                        xbrl_report = requests.get(target_url)
                        output = open(os.path.join(dir_path, file_name),'wb')
                        output.write(xbrl_report.content)
                        output.close()
                    else:
                        row = [ticker, report_type, report_year - 1, target_url]
                        csvfile = open(dir_path, 'a', encoding="utf-8", newline='')
                        write = csv.writer(csvfile)
                        write.writerow(row)
                        csvfile.close()
            except:
                pass

path = r'./Downloaded_Filings/中国海外上市公司股票总览表.csv'

base_path = "./Downloaded_Filings/"
dir_path1 = base_path + 'Oversea_company_list.csv'
field_order = ["TICKER", 'TYPE', 'YEAR', 'URL']
with open(dir_path1, 'w', encoding="utf-8", newline='') as csvfile:
    writer = csv.DictWriter(csvfile, field_order)
    writer.writeheader()
    csvfile.close()
path1 = base_path + 'Oversea_company.csv'
df0 = pd.read_csv(path1, encoding="utf_8_sig")
df = pd.read_csv(path, encoding="utf_8_sig", usecols = ['CoName','CIK','StockCode','ExchangeCode','ListDate','CoName_en','StockName','DelistDate'])
df = df.drop([0])
df = df.loc[(df['ExchangeCode'] == 'NASDAQ') | (df['ExchangeCode'] == 'NYSE')]
total = len(df0['TICKER'])
n = 0
for i in range(n, total):
    #if pd.isna(cik[i+n]):
    url_list = get_list(df0['TICKER'][i], 2009, df0['TYPE'][i])
    #else:
        #url_list = get_list(t, 2009, cik[i+n])
    if url_list == []:
        print(i+n, t, "Failed")
        row = [t, '', '', '']
        csvfile = open(dir_path1, 'a', encoding="utf-8", newline='')
        write = csv.writer(csvfile)
        write.writerow(row)
        csvfile.close()
        continue
    #dir_path = base_path + t
    # download_report(url_list, dir_path)
    download_report(url_list, dir_path1, True) #不需要原文件，只需要html地址
    print(i+n, t, "Finish!")
