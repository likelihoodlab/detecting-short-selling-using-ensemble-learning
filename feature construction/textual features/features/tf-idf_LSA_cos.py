#!/usr/bin/env python 
# -*- coding:utf-8 -*-
from nltk.text import TextCollection
from nltk import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models, similarities
import pandas as pd
import re
import pickle

# 首先，构建语料库corpus
dir_path = '../Scraping-SEC-filings-master/Downloaded_Filings/yearly_database.csv' #存放文档地址的表格
df = pd.read_csv(dir_path, encoding="utf_8_sig", dtype = {'URL': str, 'YEAR': str, 'TYPE': str, 'TICKER': str})

#sents = ['this is sentence one', 'this is sentence two', 'this is sentence three']
#sents = [word_tokenize(sent) for sent in sentences]  # 对每个句子进行分词
#print(sents)  # 输出分词后的结果
#corpus = TextCollection(docs)  # 构建语料库
#print(corpus)  # 输出语料库

# 计算语料库中"one"的tf-idf值
#tf_idf = corpus.tf_idf('one', corpus)
#print(tf_idf)

def pre_process(doc, low_freq_filter = True):
    # 对于每个文档，先进行utf-8解码，然后进行tokenize，再对每个单词小写化
    texts_tokenized = [[word.lower() for word in word_tokenize(document)] for document in doc]

    # 去停用词，用nltk带有的停用词表
    english_stopwords = stopwords.words('english')
    # 过滤掉文档中的停用词
    texts_filtered_stopwords = [[word for word in document if not word in english_stopwords] for document in
                            texts_tokenized]
    # 过滤完停用词，但是标点符号没有过滤

    # 定义一个标点符号的词典，用这个词典来过滤标点符号
    english_punctuations = [',', '.', ':', ';', '?', '!', '(', ')', '[', ']', '@', '&', '#', '%', '$', '{', '}', '--', '-','+','*','~','“','”','·','\n']
    texts_filtered = [[word for word in document if not word in english_punctuations] for document in
                  texts_filtered_stopwords]

    # 接下来将这些英文单词词干化，词干化可以提取不同语态及各种后缀的词干
    # 可以用nltk中的Lancaster Stemmer和 Poter Stemmer 工具
    # 对比发现Lancaster抽取时略去太多词尾的e，所以选Poter

    st = PorterStemmer()
    # from nltk.stem.lancaster import LancasterStemmer
    # st = LancasterStemmer()
    texts_stemmed = [[st.stem(word) for word in document] for document in texts_filtered]
    if low_freq_filter:
        #去掉文档语料库中出现次数为1的低频词
        all_stems = sum(texts_stemmed,[])
        stems_once = set(stem for stem in set(all_stems) if all_stems.count(stem) == 1)
        texts = [[stem for stem in text if stem not in stems_once] for text in texts_stemmed]
    else:
        texts = texts_stemmed
    return texts

def train_by_lsi(lib_texts, version: str, dim = 20):
    dim = str(dim)
    version = str(version)
    # 建立一个字典，字典表示了这个词以及这个词在texts语料库里面出现的次数
    dictionary = corpora.Dictionary(lib_texts)
    dictionary.save('./model/dictionary_' + version + '_' + dim + 'D.dict')
    # 把整个语料库的文档转化为（id，出现次数）
    corpus = [dictionary.doc2bow(text) for text in lib_texts]
    tfidf = models.TfidfModel(corpus)
    tfidf.save('./model/annual_report_' + version + '_' + dim + 'D.tfidf')
    corpus_tfidf = tfidf[corpus]
    # define a 20-dimensional LSI space
    lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=dim)
    lsi.save('./model/annual_report_' + version + '_' + dim + 'D .lsi')
    #lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=20)
    # transform corpus to lsi space and index it
    index = similarities.MatrixSimilarity(lsi[corpus]) # index 是 gensim.similarities.docsim.MatrixSimilarity 实例
    return (index, dictionary, lsi)
docs = []
docs_path = []
lib_texts = []
import time
t1 = time.time()
for i in range(len(df)):
    try:
        cur_path1 = './Data/' + df['TICKER'][i] + '/' + "-".join([df['TICKER'][i], df['TYPE'][i],df['YEAR'][i][:4]]) + '_pre_process.txt'
        f = open(cur_path1, 'r', encoding="utf-8")
        lib_texts.append(list(f.read().split()))
    except:
        cur_path1 = './Data/' + df['TICKER'][i] + '/' + "-".join(
            [df['TICKER'][i], df['TYPE'][i], df['YEAR'][i][:4]]) + '.txt'
        save_path = './Data/' + df['TICKER'][i] + '/' + "-".join(
            [df['TICKER'][i], df['TYPE'][i], df['YEAR'][i][:4]]) + '_pre_process.txt'
        print(save_path)
        f = open(cur_path1, 'r', encoding="utf-8")
        docs.append(f.read())
        docs_path.append(save_path)
t2 = time.time()
print(t2-t1)

for i in range(0, len(docs)):
    tmp = pre_process([docs[i]])
    lib_texts.extend(tmp)
    f = open(docs_path[i], 'w', encoding="utf-8")
    f.write(" ".join(tmp[0]))
    print(docs_path[i])
    print(lib_texts[-1][100:106])
print(lib_texts[0])
t3 = time.time()
print(t3-t2)

(index,dictionary,lsi) = train_by_lsi(lib_texts, 3)

# 相互比较
texts_lsi = []
sims = []
for j in range(len(lib_texts)):
    compare_text = dictionary.doc2bow(lib_texts[j])
    texts_lsi.append( lsi[compare_text] )
    sims.append(index[texts_lsi[j]]) #perform a similarity query against the corpus
print(sims)
df1 = pd.DataFrame(sims)
df1.to_csv('./similarity_result_20D.csv', index = 0, encoding = 'utf_8_sig')
visited = []
for i in range(len(df)):
    if df['TICKER'][i] not in visited:
        visited.append(df['TICKER'][i])
    if i + 1 == len(df) or df['TICKER'][i+1] != visited[-1]:
        df['informativeness'][i] = 1
    else:
        df['informativeness'][i] = 1 - sims[i][i+1]
print(df.head())
df.to_csv(dir_path, index = 0, encoding = 'utf_8_sig')