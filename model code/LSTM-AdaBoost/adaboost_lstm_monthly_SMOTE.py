import math
import time

import matplotlib.pyplot as plt
import numpy
from keras import backend as K
from keras.layers import Dense
from keras.layers import LSTM
from keras.models import Sequential
from keras.callbacks import Callback
from pandas import *
from sklearn.metrics import mean_squared_error, f1_score, precision_score, recall_score, roc_auc_score, roc_curve
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE
import tensorflow as tf

from adaboost import AdaBoost


# frame a sequence as a supervised learning problem
def timeseries_to_supervised(dataset, look_back=1):
    dataX, dataY = [], []
    for i in range(len(dataset)):
        if dataset[i, 3] and dataset[i, 3] > look_back:
            n = int(dataset[i, 3])
            small_dataset = dataset[i:i + n, :][::-1]
            raw_values = small_dataset[:, 4:].astype('float32')  # 月度
            scaler = MinMaxScaler(feature_range=(0, 1))
            new_dataset = scaler.fit_transform(raw_values)
            # new_dataset = raw_values
            for j in range(len(new_dataset) - look_back):
                a = new_dataset[j:(j + look_back), :-1]
                dataX.append(a)
                dataY.append(new_dataset[j + look_back, -1])
    return numpy.array(dataX), numpy.array(dataY)


def focal_loss(y, y_):
    alaph, gamma = 0.75, 2
    return -alaph * y * (1 - y_) ** gamma * K.log(y_) - (1 - alaph) * (1 - y) * y_ ** gamma * K.log(1 - y_)


def evaluate(Y, Predict, top=0.01):
    Predict = numpy.reshape(Predict, (Predict.shape[0]))
    labels = []
    for i, label in enumerate(Y):
        if label: labels.append(i)
    l = [Predict[i] for i in labels]
    print('label max pro:', max(l), len(Predict[Predict > max(l)]))
    print('label min pro:', min(l), len(Predict[Predict < min(l)]))
    print(l)

    df = pandas.DataFrame({'Y':Y, 'Predict':Predict})
    df = df.sort_values('Predict', ascending=False)
    size = int(top * len(df)) + 1
    positive = df['Y'].iloc[:size].tolist()
    # negative = df['Y'].iloc[size:].value_counts()
    TP, FP = sum(positive), len(positive) - sum(positive)
    # FN = negative[1]
    recall, precision = TP / sum(Y), TP / size
    return recall, precision


if __name__ == '__main__':
    # fix the random number seed to ensure our results are reproducible.
    numpy.random.seed(7)
    t1 = time.time()

    save_path2 = './data/monthly_database_all_features.csv'
    series = pandas.read_csv(save_path2, encoding='utf_8_sig')
    raw_values = series.values
    # reshape into X=t and Y=t+1
    look_back = 6  # 指用前一期来预测，若用前两期就改为2
    X, Y = timeseries_to_supervised(raw_values, look_back)
    X_reshape = numpy.reshape(X, (X.shape[0], -1))
    sm = SMOTE(sampling_strategy=0.01, random_state=7)
    X_res, Y_res = sm.fit_resample(X_reshape, Y)

    test_size = int(len(X) * 0.33) + 1

    trainX, testX, trainY, testY = train_test_split(X_res, Y_res, test_size=0.3, random_state=7, shuffle=True, stratify=Y_res)
    #trainX, trainY = X[:-test_size], Y[:-test_size]
    #testX, testY = X[-test_size:], Y[-test_size:]
    print(sum(trainY))
    print(sum(testY))

    # reshape trainX and testX to feed the model，ie.[samples,timesteps,features]
    trainX = numpy.reshape(trainX, (trainX.shape[0], look_back, -1)) #此处的1应该改为look_back才对
    testX = numpy.reshape(testX, (testX.shape[0], look_back, -1))

    adaboost = AdaBoost(trainX, trainY)
    errors, auc, auc_train = [], [], []
    learning_rate = 0.1
    for i in range(16):  # 指多少个弱分类器
        sample_weights = adaboost.get_weights()
        model = Sequential()
        drop = 0
        model.add(LSTM(32, activation='tanh', dropout=drop, input_shape=(trainX.shape[1], trainX.shape[2]),
                       return_sequences=True))
        model.add(LSTM(64, activation='tanh', dropout=drop))
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss=focal_loss, optimizer='adam', metrics=['accuracy'])
        model.optimizer.lr.assign(learning_rate)
        learning_rate *= 0.95
        model.summary()
        if not i:
            pre_trainPredict = model.predict(trainX)
            pre_testPredict = model.predict(testX)
            auc.append(roc_auc_score(testY, pre_testPredict))
            auc_train.append(roc_auc_score(trainY, pre_trainPredict))
        model.fit(trainX, trainY, epochs=450, batch_size=512, verbose=2, sample_weight=sample_weights)
                  #, validation_data=(testX, testY))
        adaboost.set_rule(model)
        errors.append(adaboost.evaluate())
        pre_trainPredict = adaboost.predict(trainX)
        pre_testPredict = adaboost.predict(testX)
        auc.append(roc_auc_score(testY, pre_testPredict))
        auc_train.append(roc_auc_score(trainY, pre_trainPredict))
    print("final error: ", adaboost.evaluate())
    t2 = time.time()
    print("training time:", t2-t1)

    trainPredict = adaboost.predict(trainX)
    testPredict = adaboost.predict(testX)

    evaluate(trainY, trainPredict)
    evaluate(testY, testPredict)
    fpr, tpr, thresholds = roc_curve(testY, testPredict)
    train_auc = roc_auc_score(trainY, trainPredict)
    test_auc = roc_auc_score(testY, testPredict)
    plt.figure()
    plt.plot(fpr, tpr, color='darkorange',
             lw=2, label='ROC curve (area = %0.2f)' % test_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic')
    plt.legend(loc="lower right")
    plt.savefig("roc.png")
    plt.show()
    print('train:', train_auc)
    print('test:', test_auc)
    plt.figure()
    plt.scatter(range(len(errors)), errors)
    plt.show()
    plt.figure()
    plt.plot(range(1, 1 + len(auc)), auc, color='navy', linestyle='--', label='AUC of the test set', lw=2)
    plt.plot(range(1, 1 + len(auc_train)), auc_train, color='darkorange', label='AUC of the train set', lw=2)
    plt.legend()
    plt.show()
'''

    # invert predictions
    scaler = MinMaxScaler(feature_range=(0, 1))
    trainPredict = scaler.inverse_transform(trainPredict)
    trainY = scaler.inverse_transform([trainY])
    testPredict = scaler.inverse_transform(testPredict)
    testY = scaler.inverse_transform([testY])

    # calculate root mean squared error
    trainScore = math.sqrt(mean_squared_error(trainY[0], trainPredict[:, 0]))
    print('Train Score: %.2f RMSE' % (trainScore))
    testScore = math.sqrt(mean_squared_error(testY[0], testPredict[:, 0]))
    print('Test Score: %.2f RMSE' % (testScore))

    trainPredictPlot = numpy.empty_like(raw_values)
    trainPredictPlot[:, :] = numpy.nan
    trainPredictPlot[0, :] = t0
    trainPredictPlot[look_back:len(trainPredict) + look_back, :] = trainPredict

    # shift test predictions for plotting
    testPredictPlot = numpy.empty_like(raw_values)
    testPredictPlot[:, :] = numpy.nan
    testPredictPlot[len(trainPredict) + look_back:, :] = testPredict

    # plot baseline and predictions
    plt.plot(raw_values[-len(testPredict):, :], 'k-', linewidth=1.0)
    # plt.plot(trainPredictPlot, 'k-.')
    plt.plot(testPredictPlot[-len(testPredict):, :], 'k--', linewidth=1.0)
    plt.show()
'''
