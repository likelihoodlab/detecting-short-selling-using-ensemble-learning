import math
import time
import pandas as pd
from sklearn.ensemble import AdaBoostClassifier
from sklearn import metrics
import matplotlib.pyplot as plt
import numpy
from keras import backend as K
from keras.layers import Dense
from keras.layers import LSTM
from keras.models import Sequential
from keras.callbacks import Callback
from pandas import *
from sklearn.metrics import mean_squared_error, f1_score, precision_score, recall_score, roc_auc_score, roc_curve
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
import tensorflow as tf
from imblearn.over_sampling import SMOTE
from sklearn.model_selection import KFold




def evaluate(Y, Predict, top=0.01):
        #Predict = numpy.reshape(Predict, (Predict.shape[0]))
        labels = []
        for i, label in enumerate(Y):
            if label: labels.append(i)
        l = [Predict[i] for i in labels]
        # print('label max pro:', max(l), len(Predict[Predict > max(l)]))
        # print('label min pro:', min(l), len(Predict[Predict < min(l)]))
        # print(l)

        df = pd.DataFrame({'Y': Y, 'Predict': Predict})
        df = df.sort_values('Predict', ascending=False)
        size = int(top * len(df)) + 1
        positive = df['Y'].iloc[:size].tolist()
        # negative = df['Y'].iloc[size:].value_counts()
        df['Y'].iloc[size:].value_counts()
        TP, FP = sum(positive),len(positive)-sum(positive)
        # FN = negative[1]
        recall, precision = TP / sum(Y), TP / size
        return recall, precision

def getavg(array):
    return sum(array)/len(array)


def timeseries_to_supervised(dataset, look_back):
    dataX, dataY = [], []
    for i in range(len(dataset)):
        if dataset[i, 2] and dataset[i, 2] > look_back:
            n = int(dataset[i, 2])
            small_dataset = dataset[i:i + n, :][::-1]
            raw_values = small_dataset[:, 6:35].astype('float32')  # 年度
            scaler = MinMaxScaler(feature_range=(0, 1))
            new_dataset = scaler.fit_transform(raw_values)
            # new_dataset = raw_values
            for j in range(len(new_dataset) - look_back):
                a = new_dataset[j:(j + look_back), :-1]
                dataX.append(a)
                dataY.append(new_dataset[j + look_back, -1])
    return numpy.array(dataX), numpy.array(dataY)

if __name__ == '__main__':

    print("Start read data...")
    series = pd.read_csv('E:\programming\python\selected_data-11.11.csv', encoding='utf_8_sig')
    raw_values = series.values
    # reshape into X=t and Y=t+1
    look_back = 2
    features, labels = timeseries_to_supervised(raw_values, look_back)
    nsamples, nx, ny = features.shape
    X_res = features.reshape((nsamples, nx * ny))
    #sm = SMOTE(sampling_strategy=0.025, random_state=7)
    #X_res, labels = sm.fit_resample(X_res, labels)
    clf: AdaBoostClassifier = AdaBoostClassifier(n_estimators=16, algorithm='SAMME')

    test_size = int(len(features) * 0.33) + 1
    kf = KFold(3, True, 7)
    avg_train_auc, avg_train_recall, avg_train_precision = [], [], []
    avg_test_auc, avg_test_recall, avg_test_precision = [], [], []
    for train_index, test_index in kf.split(X_res):
        train_features, test_features = X_res[train_index], X_res[test_index]
        train_labels, test_labels = labels[train_index], labels[test_index]
        print(sum(train_labels))
        print(sum(test_labels))
        clf.fit(train_features, train_labels)
        time_1 = time.time()
        print('Start training...')


        test_predict = clf.predict(test_features)
        train_predict = clf.predict(train_features)
        time_2 = time.time()
        print('training cost %f seconds' % (time_2 - time_1))
        #预测概率
        trainPredict = clf.predict_proba(train_features)[:,1]
        testPredict = clf.predict_proba(test_features)[:,1]



        print("\t预测结果评价报表：\n", metrics.classification_report(test_labels, test_predict))
        print("\t混淆矩阵：\n", metrics.confusion_matrix(test_labels, test_predict))
        test_recall, test_precision = (evaluate(test_labels, testPredict))
        print("测试集的recall和precision：",test_recall,test_precision)
        train_recall, train_precision = (evaluate(train_labels, trainPredict))
        print("训练集的recall和precision：",train_recall,train_precision)

        fpr, tpr, thresholds = roc_curve(test_labels, testPredict)
        train_auc = roc_auc_score(train_labels, trainPredict)
        test_auc = roc_auc_score(test_labels, testPredict)
        avg_train_recall.append(train_recall)
        avg_train_precision.append(train_precision)
        avg_test_recall.append(test_recall)
        avg_test_precision.append(test_precision)
        avg_train_auc.append(train_auc)
        avg_test_auc.append(test_auc)



        plt.figure()
        plt.plot(fpr, tpr, color='darkorange',
                 lw=2, label='ROC curve (area = %0.2f)' % test_auc)
        plt.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic')
        plt.legend(loc="lower right")
        plt.savefig("roc.png")
        plt.show()
        print('train:', train_auc)
        print('test:', test_auc)

    print("平均训练集auc：",getavg(avg_train_auc))
    print("平均测试集auc：",getavg(avg_test_auc))
    print("平均训练集recall：",getavg(avg_train_recall))
    print("平均测试集recall:",getavg(avg_test_recall))
    print("平均训练集precision：",getavg(avg_train_precision))
    print("平均测试集precision：",getavg(avg_test_precision))